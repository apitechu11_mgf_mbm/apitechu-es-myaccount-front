import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {} from '@polymer/polymer/lib/elements/dom-if.js';

import '@polymer/iron-ajax/iron-ajax';
import '@polymer/iron-pages/iron-pages';
import '@polymer/app-route/app-route';
import '@polymer/iron-icons/iron-icons';
import '@polymer/paper-icon-button/paper-icon-button';
import '@polymer/app-layout/app-header-layout/app-header-layout';
import '@polymer/app-layout/app-header/app-header';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout';
import '@polymer/app-layout/app-drawer/app-drawer';
import '@polymer/app-layout/app-toolbar/app-toolbar';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects';

import '../visor-cuentas/visor-cuentas.js';

import '../visor-mov/visor-mov.js';
import '../crear-cuentas/crear-cuentas.js';

import '../transfe-cuentas/transfe-cuentas.js'
import '../ingreso-cuenta/ingreso-cuenta.js'
import '../reintegro-cuenta/reintegro-cuenta.js'

/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
        app-header {
          color: #fff;
          background-color: #003DB3;
  
          --app-header-background-front-layer: {
            background-image: url('/resources/img/login-background.jpg');
            background-position: 50% 10%;
          }
        }
        .user {
          font-size: 17px;
        }
        paper-icon-button {
          --paper-icon-button-ink-color: white;
        }
        .menu {
          padding-left: 16px;
        }
        ul {
          list-style: none;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <!-- select value="{{component::change}}" placeholder="Operativa">
        <option value=" ">Operativa</option>
        <option value="miscuentas">Posición Global</option>
        <option value="crearcuenta">Nueva Cuenta</option>
        <option value="mismovimientos">Movimientos</option>
      </select -->

      <div class="container">
      <div class="row">
      <div class="col-md-12">

      <span hidden$="{{!isAccountSelected}}">
        <select value="{{component::change}}" placeholder="Operativa">
          <option value="miscuentas">Posición Global</option>
          <option value="mismovimientos">Consultar Movimientos</option>
          <option value="transfer">Hacer una transferencia</option>
          <option value="income">Hacer un ingreso</option>
          <option value="refund">Hacer un reintegro</option>
        </select>
      </span>

      </div>
      </div>
      </div>

      <iron-pages role="main" selected="[[component]]" attr-for-selected="name" selected-attribute="visible">
        <visor-cuentas id="visorcuentas" name="miscuentas"     route={{route}}
          on-evtselecteditem="processSelectAccount"
        ></visor-cuentas>
        <visor-mov     id="visormovs"    name="mismovimientos" route={{route}} iban=[[account.iban]]
        ></visor-mov>
        <crear-cuentas id="altacuenta"   name="crearcuenta" route={{route}}
          on-evtnewaccountsusceed="processNewAccountSusceed"
          on-evtnewaccounterror="processNewAccountError"
        ></crear-cuentas>
        <transfe-cuentas id="transferencia"    name="transfer" route={{route}} iban=[[account.iban]]
          on-evttransfersusceed="processTransferSusceed"
        ></transfe-cuentas>
        <ingreso-cuenta id="ingreso"     name="income" route={{route}} iban=[[account.iban]]
          on-evtingresosusceed="processIngresoSusceed"
        ></ingreso-cuenta>
        <reintegro-cuenta id="reintegro" name="refund" route={{route}} iban=[[account.iban]]
          on-evtreintegrosusceed="processReintegroSusceed"
        ></reintegro-cuenta>
      </iron-pages>

      <iron-ajax
        id="getUser"
        headers="[[headers]]"
        url="http://localhost:3000/techu/v1/users/{{userid}}"
        handle-as="json"
        method="GET"
        content-type="application/json"
        on-response="tratarRespuestaUser"
        on-error="tratarErrorUser"
      ></iron-ajax>
    `;
  }

  static get properties() {
    return {
      first_name: {
        type: String
      }, 
      last_name: {
        type: String
      }, 
      email: {
        type: String
      }, 
      userid: {
        type: String,
        observer: '_useridChanged'
      },
      token: {
        type: String,
        value: null
      },
      headers: {
        computed: '_computeHeader(token)',
        observer: "_headersChanged"
      },
      viewPages: {
        type: Boolean,
        value: false
      },
      route: {
        type: Object
      },
      component: {
        type: String,
        observer: '_componentChanged',
        value: "miscuentas"
      },
      visible: {
        type: Boolean,
        value: false,
        observer: '_visibleChanged'
      },
      isAccountSelected: {
        type: Boolean,
        value: false
      },
      account:{
        type: Object
      }
    };
  } // End properties

  constructor() {
    console.log("VisorUsuario.constructor");
    super();
  }

  connectedCallback() {
    console.log("VisorUsuario.connectedCallback");
    super.connectedCallback();
    const user = JSON.parse(window.sessionStorage.getItem('user'));
    console.log(user);
    if (user != null) {
      this.userid = user.userId;
    }
    this.token = window.sessionStorage.getItem('userToken');
  }

  disconnectedCallback() {
    console.log("VisorUsuario.disconnectedCallback");
    super.disconnectedCallback();
  }

  _computeHeader(token) {
    console.log("VisorUsuario._computeHeader");
    console.log(token);
    if (token == undefined) {
      return null;
    } else {
      var header = {
        jwt: token
      }
      console.log(header);
      return header;
    }
  } // End _computeHeader

  _headersChanged(newValue, oldValue) {
    console.log("VisorUsuario._headersChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);
    if (newValue != undefined) {
      this.sincronizaPeticion(newValue, this.id);
    }
  } // End _headersChanged (observer)

  _useridChanged(newValue, oldValue) {
    console.log("VisorUsuario._useridChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);
    if (newValue != undefined) {
      this.sincronizaPeticion(this.headers, newValue);
    }
  } // End _useridChanged (observer)

  _componentChanged(newValue, oldValue) {
    console.log("VisorUsuario._componentChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);
    if (newValue != undefined) {
      this.sincronizaPeticion(this.headers, newValue);
    }
  } // End _componentChanged (observer)

  sincronizaPeticion(headers, userid) {
    console.log("VisorUsuario.sincronizaPeticion");
    console.log(headers);
    console.log(userid);
    
    if (headers == undefined) {
      this.token = window.sessionStorage.getItem('userToken');
    } else {
      if (headers != undefined && this.headers == headers && userid != undefined && this.userid == userid) {
        this.$.getUser.generateRequest();
        console.log("Petición enviada");
      }
    }
  }

  tratarRespuestaUser(data) {
    console.log("VisorUsuario.tratarRespuestaUser");
    console.log(data.detail.response);

    this.first_name = data.detail.response.first_name;
    this.last_name = data.detail.response.last_name;
    this.email = data.detail.response.email;

    this.dispatchEvent(
      new CustomEvent(
        "evtdetailusersusceed",
        {
          "detail": {
            "user": {
              "userId"    : data.detail.response.id,
              "userType"  : data.detail.response.type,
              "first_name": data.detail.response.first_name,
              "last_name" : data.detail.response.last_name,
              "email"     : data.detail.response.email,
              "home"      : data.detail.response.home
            },
            "isLogged"  : data.detail.response.logged
          }
        }
      )
    );
  } // End tratarRespuestaUser

  tratarErrorUser(error) {
    console.log("VisorUsuario.tratarErrorUser");
    console.log(error);
  } // End tratarErrorUser

  ocultarCuentas () {
    this.$.visorcuentas.accounts = [];
    this.$.visorcuentas.userid = null;
    this.$.visorcuentas.token = null;
    this.viewPages = false;
  }

  _visibleChanged(visible) {
    if (visible) {
      console.log('visor-usuario visible');
    }
  }

  _showBackButton(page) {
    return page == 'session';
  }
  
  processSelectAccount(event) {
    console.log("VisorUsuario.processSelectAccount");
    console.log(event.detail); 
    this.isAccountSelected = event.detail.isAccountSelected;
    this.account = event.detail.account;

  } // End processDetailUser

  processNewAccountSusceed(event) {
    console.log("VisorUsuario.processNewAccountSusceed");
    console.log(event);

    
    this.dispatchEvent(
      new CustomEvent(
        "evtnewaccountsusceed",
        { 
          "detail": {
            "msg": event.detail.msg,
            "iban": event.detail.iban
          }
        }
      )
    );
  }

  processNewAccountError(event) {
    console.log("CrearCuentas.processNewAccountError");
    console.log(event);

    this.dispatchEvent(
      new CustomEvent(
        "evtnewaccounterror",
        { 
          "detail": {
            "msg": event.detail.msg,
            "status": event.detail.status
          }
        }
      )
    );
  }

  processIngresoSusceed(event) {
    console.log("VisorUsuario.processNewAccountSusceed");
    console.log(event);
    
    this.dispatchEvent(
      new CustomEvent(
        "evtingresosusceed",
        { 
          "detail": {
            "msg": event.detail.msg
          }
        }
      )
    );
  }

  processReintegroSusceed(event) {
    console.log("VisorUsuario.processReintegroSusceed");
    console.log(event);
    
    this.dispatchEvent(
      new CustomEvent(
        "evtreintegrosusceed",
        { 
          "detail": {
            "msg": event.detail.msg
          }
        }
      )
    );
  }

  processTransferSusceed(event) {
    console.log("VisorUsuario.processTransferSusceed");
    console.log(event);
    
    this.dispatchEvent(
      new CustomEvent(
        "evttransfersusceed",
        { 
          "detail": {
            "msg": event.detail.msg
          }
        }
      )
    );
  }

}

window.customElements.define('visor-usuario', VisorUsuario);
