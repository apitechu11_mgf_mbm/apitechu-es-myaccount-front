import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';


/**
 * @customElement
 * @polymer
 */
class LogoutUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all: initial;*/
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <!-- button class="btn btn-dark" on-click="logout">Logout</button -->

      <iron-ajax
        id="doLogout"
        headers="[[headers]]"
        url="http://localhost:3000/techu/v1/logout/{{userid}}"
        handle-as="json"
        method="POST"
        content-type="application/json"
        on-response="tratarRespuestaLogout"
        on-error="tratarErrorLogout"
      ></iron-ajax>
    `;
  } // End template

  static get properties() {
    return {
      initialized: {
        type: Boolean
      },
      userid: {
        type: String,
        observer: '_useridChanged'
      },
      token: {
        type: String
      },
      headers: {
        computed: '_computeHeader(token)',
        observer: "_headersChanged"
      }
    };
  } // End properties

  constructor() {
    super();
    this.initialized = false;
    console.log("LogoutUsuario.constructor");
  }

  connectedCallback() {
    if (!this.initialized) {
      const user = JSON.parse(window.sessionStorage.getItem('user'));
      if(user != null){
        this.userid = user.userId;
      }
      this.token = window.sessionStorage.getItem('userToken');
      this.initialized = true;
      console.log("LogoutUsuario.connectedCallback initialized");
    }
    super.connectedCallback();
    console.log("LogoutUsuario.connectedCallback");
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    console.log("LogoutUsuario.disconnectedCallback");
  }

  attributeChangedCallback(name, oldVal, newVal) {
    console.log("LogoutUsuario.attributeChangedCallback");
    //this[name] = newVal;
  }

  _computeHeader(token) {
    console.log("LogoutUsuario._computeHeader");
    console.log(token);
    if (token == undefined) {
      return null;
    } else {
      var header = {
        jwt: token
      }
      console.log(header);
      return header;
    }
  }

  _headersChanged(newValue, oldValue) {
    console.log("LogoutUsuario._headersChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);
    if (newValue != undefined) {
      if (this.token != undefined){
        this.sincronizaPeticion(newValue, this.id);
      }
    }
  } // End _headersChanged (observer)

  _useridChanged(newValue, oldValue) {
    console.log("LogoutUsuario._useridChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);

    if (newValue != undefined) {
      this.sincronizaPeticion(this.headers, newValue);
    }
  } // End _useridChanged (observer)

  sincronizaPeticion(headers, userid) {
    console.log("LogoutUsuario.sincronizaPeticion");
    console.log(headers);
    console.log(userid);
    
    var page = window.location.pathname.split('/')[1];
    console.log(page);

    if (headers != undefined && this.headers == headers && 
      userid != undefined && this.userid == userid &&
      page == 'logout') {
      this.logout();
    }
  }

  logout(){
    console.log("LogoutUsuario.logout");

    var logoutData = {}

    this.$.doLogout.body = JSON.stringify(logoutData);
    if (this.headers != undefined && this.userid != undefined) {
      this.$.doLogout.generateRequest();
      console.log("Petición enviada");
    }
  } // End logout

  tratarRespuestaLogout(data) {
    console.log("LogoutUsuario.tratarRespuestaLogout");
    console.log(data.detail.response);

    window.sessionStorage.clear();
    top.location = "/index.html";

    this.dispatchEvent(
      new CustomEvent(
        "evtlogoutsusceed",
        {
          "detail": {
            "userId" : data.detail.response.id,
            "isLogged" : false
          }
        }
      )
    )
  } // End tratarRespuestaLogout

  tratarErrorLogout(error) {
    console.log("LogoutUsuario.tratarErrorLogout");
    console.log(error);
    this.userid = "";
  } // End tratarErrorLogout

} // End class

window.customElements.define('logout-usuario', LogoutUsuario);
