import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-input/paper-input';

import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-text-field/vaadin-email-field.js';

/**
 * @customElement
 * @polymer
 */
class IngreAccount extends PolymerElement {
  static get template() {
    return html`
    <style>
      :host {
        display: block;
      }
      @media (min-width: 1200px) {
        .container{
          max-width: 1200px;
        }
      }
    </style>
    <custom-style>
    <style is="custom-style">
      .custom-parent {
        font-size: 12px;
      }
      paper-input.custom {
        margin-bottom: 14px;
        --primary-text-color: #01579B;
        --paper-input-container-color: black;
        --paper-input-container-focus-color: black;
        --paper-input-container-invalid-color: black;
        border: 1px solid #BDBDBD;
        border-radius: 5px;

        /* Reset some defaults */
        --paper-input-container: { padding: 0;};
        --paper-input-container-underline: { display: none; height: 0;};
        --paper-input-container-underline-focus: { display: none; };

        /* New custom styles */
        --paper-input-container-input: {
          box-sizing: border-box;
          font-size: inherit;
          padding: 4px;
        };
        /*--paper-input-container-input-focus: {
          background: rgba(0, 0, 0, 0.1);
        };
        --paper-input-container-input-invalid: {
          background: rgba(255, 0, 0, 0.3);
        };*/
        --paper-input-container-label: {
          top: -8px;
          left: 4px;
          background: white;
          padding: 2px;
          font-weight: bold;
        };
        --paper-input-container-label-floating: {
          width: auto;
        };
      }
    </style>
    </custom-style>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <div class="container">
    <div class="row">
    <div class="col-md-12">

    <h2>Ingreso en cuenta <strong>{{iban}}</strong></h2>    
    <div class="row">
      <div class="custom-parent col-md-8 mb-3">
        <paper-input class="custom" id="concepto" label="Concepto"  value="{{Concepto::input}}" required always-float-label auto-validate  error-message="Concepto obligatorio">
        </paper-input>
      </div>
    </div>

    <div class="row">
      <div class="custom-parent col-md-2 mb-3">
        <paper-input class="custom" id="importe" label="Importe" value="{{Importe::input}}" required maxlength="8" always-float-label auto-validate  error-message="Importe obligatorio">
        </paper-input>
      </div>
    </div>

    <div class="row">
      <div class="custom-parent col-md-8 mb-3">
        <button class="btn btn-lg btn-primary btn-block" on-click="ingreso">Realizar ingreso</button>
      </div>
    </div>

    </div>
    </div>
    </div>

    <iron-ajax
      id="doIngreso"
      headers="[[headers]]"
      url="http://localhost:3000/techu/v1/ingreso/{{userid}}"
      handle-as="json"
      method="POST"
      content-type="application/json"
      on-response="manageAJAXResponse"
      on-error="showError"
    ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      userid: {
        type: String,
        observer: '_useridChanged'
      },
      token: {
        type: String
      },
      headers: {
        computed: '_computeHeader(token)',
        observer: "_headersChanged"
      },
      iban:{
        type:String,
        observer: '_ibanChanged'
      },
      Destino:{
        type:String
      },
      Concepto: {
        type: String
      },
      Importe:{
        type:Number   
      }
    };
  } // Fin de prpperties

  constructor() {
    console.log("IngreAccount.constructor");
    super();
  }

  connectedCallback() {
    console.log("IngreAccount.connectedCallback");
    super.connectedCallback();
    const user = JSON.parse(window.sessionStorage.getItem('user'));
    console.log(user);
    if (user != null) {
      this.userid = user.userId;
    }
    this.token = window.sessionStorage.getItem('userToken');
  }

  disconnectedCallback() {
    console.log("IngreAccount.disconnectedCallback");
    super.disconnectedCallback();
  }

  _computeHeader(token) {
    console.log("IngreAccount._computeHeader");
    console.log(token);
    if (token == undefined) {
      return null;
    } else {
      var header = {
        jwt: token
      }
      console.log(header);
      return header;
    }
  }

  _headersChanged(newValue, oldValue) {
    console.log("IngreAccount._headersChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);
  } // End _headersChanged (observer)

  _useridChanged(newValue, oldValue) {
    console.log("IngreAccount._useridChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);
  } // End _useridChanged (observer)

  _ibanChanged(newValue, oldValue) {
    console.log("IngreAccount._ibanChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);
  } // End _ibanChanged (observer)

  ingreso(){
    console.log("IngreAccount.ingreso");

    var ingresoData ={
      "iban" : this.iban,
      "Concepto" : this.Concepto,
      "Importe" : this.Importe
    }
    console.log(ingresoData);

    this.$.doIngreso.body = JSON.stringify(ingresoData);  //lo trasnformo en json los datos
    this.$.doIngreso.generateRequest();
    console.log ("Peticion enviada");
  }

  showError(error){
    console.log("Hubo un error");
    console.log(error);
  }

  manageAJAXResponse(data){
    console.log("manageAJAXResponse");
    console.log(data.detail.response);
    this.Concepto = null;
    this.Importe = null;

    this.dispatchEvent(
      new CustomEvent(
        "evtingresosusceed",
        { 
          "detail": {
            "msg": data.detail.response.msg
          }
        }
      )
    );
  }
}  //End de la clase


window.customElements.define('ingreso-cuenta', IngreAccount);
