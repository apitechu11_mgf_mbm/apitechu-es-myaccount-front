import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';


/**
 * @customElement
 * @polymer
 */
class ForgetPasswd extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }
        paper-input {
          margin-bottom: 4px;
          --paper-input-container-label-floating: {
          }
        }
      </style>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <span hidden$="{{!existe}}">
  <!--  <div class="container">
    	<div class="row" style="margin-top:60px;">
    		<div class="col-md-4 col-md-offset-4"> -->
    	 <div class="alert alert-success alert-dismissible" role="alert">
    	 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    			    <strong>Nueva Password enviada! </strong>Por favor, checkea tu email para instrucciones tras el reseteo de tu password.
    		</div>
   </span>
    <div class="container">

			<form id="form1" class="form-signin">
				<fieldset>
			  	<h2>Solicitud de Reseteo de password</h2>
			  	<hr class="colorgraph">
				<p><input class="form-control" placeholder="Introduzca su E-mail" name="email" type="email" required value="{{email::input}}"> </p>
				<p>
        <button class="btn btn-lg btn-primary btn-block" type="button" on-click="send" placeholder="Introduzca su E-mail">
        Introduzca su Email</button>
			</p>
			</fieldset>
		  </form>
		</div>
  	</div>
</div>

    <iron-ajax
      id="sendMail"
      url="http://localhost:3000/techu/v1/forgetmail"
      handle-as="json"
      method="POST"
      content-type="application/json"
      on-response="tratarRespuestaMail"
      on-error="tratarErrorMail"
    ></iron-ajax>
    `;
  }

  static get properties() {
    return {
      email: {
        type: String
      },
      loading: {
        type: Boolean,
        value: false
      },
      invalidEmail: Boolean,
      existe:{
        type:Boolean,
        value:false
      }
    };
  } // Fin de prpperties


  send(){
    console.log("Envio reseteo de password");
    var loginData = {
      "email" : this.email
    }
  //  var loginData = this.email;
    console.log(loginData);
    this.$.sendMail.body = JSON.stringify(loginData);
    this.$.sendMail.generateRequest();
    console.log("Petición enviada");
  }

  tratarRespuestaMail(data) {
    console.log("LoginUsuario.tratarRespuestaLogin");
    console.log(data.detail.response);
    //this.existe = true;
    this.dispatchEvent(
      new CustomEvent(
        "evtresetsusceed",
        {
          "detail": {
            "msg": "Password enviada! Checkea tu email para seguir las instrucciones",
            "user": this.email
          }
        }
      )
    );
//    document.getElementById("form1").reset();
//    document.forms['form1'].reset();
    this.$.form1.reset();
  }

  tratarErrorMail(error) {
    console.log("ReseteoUsuario.tratarErrorMail");
    console.log(error);

    const req = error.detail.request;
    console.log("veo",req.status);

    if (req.status == 400 || req.status == 401 || req.status == 403) {
      this.dispatchEvent(
        new CustomEvent(
          "evtreseterror", {
            "detail": {
              "msg"    : 'Usuario no existente...',
              "status" : req.status
            }
          }
        )
      );
    } else if (req.status == 500) {
      this.dispatchEvent(
        new CustomEvent(
          "evtreseterror", {
            "detail": {
              "msg"    : 'Por favor, pruebe mas tarde...',
              "status" : req.status
            }
          }
        )
      );
    } else {
      console.log(req.status);
    }
    this.$.form1.reset();
  //  this.dispatch('Error en el cambio de password');
  }

  clearInputFields() {
    document.getElementById("form1").reset();
  }

}  //End de la clase


window.customElements.define('forget-passwd', ForgetPasswd);
