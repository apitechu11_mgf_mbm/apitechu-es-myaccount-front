import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-selection-column.js';

import '@vaadin/vaadin-button';
import '@vaadin/vaadin-grid';
import '@vaadin/vaadin-checkbox';
import '@vaadin/vaadin-text-field';

/**
 * @customElement
 * @polymer
 */
class VisorCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <div class="container">
      <div class="row">
      <div class="col-md-12">
  
      <h2>Listado de cuentas (Posición Global)</h2>
  
      <vaadin-grid id="cuentas" height-by-rows theme="row-stripes" active-item="{{activeItem}}">
      <!-- vaadin-grid-selection-column auto-select frozen></vaadin-grid-selection-column -->
        <vaadin-grid-column path="iban" width="35%" flex-grow="0" header="IBAN"></vaadin-grid-column>
        <vaadin-grid-column path="alias" width="30%" flex-grow="0"  header="alias de tu cuenta"></vaadin-grid-column>
        <vaadin-grid-column path="balance" width="15%" text-align="end" header="Saldo"></vaadin-grid-column>
        <vaadin-grid-column path="divisa" width="15%" text-align="center" header="Divisa"></vaadin-grid-column>
      </vaadin-grid>

      </div>
      </div>
      </div>
  
      <iron-ajax
        id="getAccounts"
        headers="[[headers]]"
        url="http://localhost:3000/techu/v1/accounts/{{userid}}"
        handle-as="json"
        method="GET"
        content-type="application/json"
        on-response="tratarRespuestaAccounts"
        on-error="tratarErrorAccounts"
      ></iron-ajax>
    `;
  }

  static get properties() {
    return {
      userid: {
        type: String,
        observer: '_useridChanged'
      },
      token: {
        type: String
      },
      headers: {
        computed: '_computeHeader(token)',
        observer: "_headersChanged"
      },
      accounts: {
        type: Array
      },
      activeItem: {
        observer: '_activeItemChanged'
      }

    };
  }

  constructor() {
    console.log("VisorCuentas.constructor");
    super();
  }

  connectedCallback() {
    console.log("VisorCuentas.connectedCallback");
    super.connectedCallback();
    const user = JSON.parse(window.sessionStorage.getItem('user'));
    console.log(user);
    if (user != null) {
      this.userid = user.userId;
    }
    this.token = window.sessionStorage.getItem('userToken');
  }

  disconnectedCallback() {
    console.log("VisorCuentas.disconnectedCallback");
    super.disconnectedCallback();
  }

  _computeHeader(token) {
    console.log("VisorCuentas._computeHeader");
    console.log(token);
    if (token == undefined) {
      return null;
    } else {
      var header = {
        jwt: token
      }
      console.log(header);
      return header;
    }
  }

  _headersChanged(newValue, oldValue) {
    console.log("VisorCuentas._headersChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);
    if (newValue != undefined) {
      this.sincronizaPeticion(newValue, this.userid);
    }
  } // End _headersChanged (observer)

  _useridChanged(newValue, oldValue) {
    console.log("VisorCuentas._useridChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);
    if (newValue != undefined) {
      this.sincronizaPeticion(this.headers, newValue);
    }
  } // End _useridChanged (observer)

  sincronizaPeticion(headers, userid) {
    console.log("VisorCuentas.sincronizaPeticion");
    console.log(headers);
    console.log(userid);

    if (headers == undefined) {
      this.token = window.sessionStorage.getItem('userToken');
    } else {
      if (headers != undefined && this.headers == headers && userid != undefined && this.userid == userid) {
        this.$.getAccounts.generateRequest();
        console.log("Petición enviada");
      }
    }
  }

  _activeItemChanged(newValue, oldValue) {
    console.log("VisorCuentas._useridChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);
    if (newValue != undefined) {
      this.sincronizaPeticion(this.headers, newValue);
    }
  } // End _useridChanged (observer)

  _activeItemChanged(item) {
    console.log("_activeItemChanged: ");
    //console.log(item);
    
    this.$.cuentas.selectedItems = item ? [item] : [];
    console.log(this.$.cuentas.selectedItems)

    if (item) {
      this.dispatchEvent(
        new CustomEvent(
          "evtselecteditem",
          {
            "detail": { 
              "account": this.$.cuentas.selectedItems[0],
              "isAccountSelected" : true
            }
          }
        )
      );
    } else {
      this.dispatchEvent(
        new CustomEvent(
          "evtselecteditem",
          {
            "detail": { 
              "account": null,
              "isAccountSelected" : false
            }
          }
        )
      );  
    }
  }

  tratarRespuestaAccounts(data) {
    console.log("VisorCuentas.tratarRespuestaAccounts");
    console.log(data.detail.response);

    this.accounts = data.detail.response;
    this.$.cuentas.items = this.accounts;
  
    //this.$.cuentas.selection.mode = "single";

    //this.$.cuentas.addEventListener("selected-items-changed", function() {
    //  console.log("Selected: " + this.$.cuentas.selection.selected());
    //  console.log("Deselected: " + this.$.cuentas.selection.deselected());
    //});

  } // End tratarRespuestaAccounts

  tratarErrorAccounts(error) {
    console.log("VisorCuentas.tratarErrorAccounts");
    console.log(error);
  } // End tratarErrorAccounts

}

window.customElements.define('visor-cuentas', VisorCuentas);
