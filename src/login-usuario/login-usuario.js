import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-input/paper-input';

/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }
        paper-input {
          margin-bottom: 4px;
          --paper-input-container-label-floating: {
          }
        }
        .texto-medio {
          font-size: 20px;
        }
      </style>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <div class="container">
    <div class="row">
    <div class="col-md-12">

    <h2>Login Usuario</h2>
    <div class="row">
      <div class="custom-parent col-md-12 mb-3">
      <iron-icon icon="mail" slot="prefix"></iron-icon>
      <paper-input id="inputEmail" label="Email address" type="email"  required auto-validate error-message="Introduzca un email valido"
      value="{{email::input}}" invalid="{{invalidEmail}}"></paper-input>
      </div>
      <div class="custom-parent col-md-12 mb-3">
      <paper-input id="inputPassword" label="Password" type="password" required auto-validate error-message="Introduzca su password"
      value="{{password::input}}" invalid="{{invalidPassword}}"></paper-input>
      </div>
    </div>
    <p>     <a class="pull-right texto-medio" href="/forgetpassword">Recuerdame la password?</a> </p>
    <button class="btn btn-lg btn-primary btn-block" type="button" on-click="login" disabled$="[[buttonDisabled]]">Login</button>

    <a href="/newuser">
      <p class="mt-3 mb-3 text-muted texto-medio">¿Usuario No registrado? Crear una cuenta nueva...</p>
    </a>
    
    </div>
    </div>
    </div>

    <iron-ajax
      id="doLogin"
      url="http://localhost:3000/techu/v1/login/"
      handle-as="json"
      method="POST"
      content-type="application/json"
      on-response="tratarRespuestaLogin"
      on-error="tratarErrorLogin"
      loading="{{loading}}"
    ></iron-ajax>
    `;
  }

  static get properties() {
    return {
      email: {
        type: String
      },password:{
        type:String
      },isLogged:{
        type:Boolean,
        value:false
      },
      loading: {
        type: Boolean,
        value: false
      },
      invalidEmail: Boolean,
      invalidPassword: Boolean,
      buttonDisabled: {
        type: Boolean,
        value: true,
        computed: '_isButtonDisabled(loading, invalidEmail, invalidPassword)'
      }
    };
  } // Fin de prpperties

  _isButtonDisabled(loading, invalidEmail, invalidPassword) {
    return loading || invalidEmail || invalidPassword;
  }

  _validateForm() {
    return this.shadowRoot.querySelector('#inputEmail').validate() &&
           this.shadowRoot.querySelector('#inputPassword').validate();
  }

  login(){
    console.log("LoginUsuario.login");
    if (this._validateForm()) {
      var loginData = {
        "email" : this.email,
        "password" : this.password
      }
      console.log(loginData);

      this.isLogged = false;

      this.$.doLogin.body = JSON.stringify(loginData);
      this.$.doLogin.generateRequest();
      console.log("Petición enviada");
    }
  }

  tratarRespuestaLogin(data) {
    console.log("LoginUsuario.tratarRespuestaLogin");
    console.log(data.detail.response);

    this.isLogged = true;
    // this.clearInputFields();

    this.dispatchEvent(
      new CustomEvent(
        "evtloginsusceed",
        {
          "detail": {
            "user": {
              "userId"    : data.detail.response.id,
              "userType"  : data.detail.response.type,
              "email"     : this.email
            },
            "userToken" : data.detail.response.jwt,
            "isLogged"  : true
          }
        }
      )
    );
  }

  tratarErrorLogin(error) {
    console.log("LoginUsuario.tratarErrorLogin");
    console.log(error);

    const req = error.detail.request;

    if (req.status == 400 || req.status == 401 || req.status == 403) {
      this.dispatchEvent(
        new CustomEvent(
          "evtloginerror", {
            "detail": {
              "msg"    : 'Por favor, revise sus credenciales...',
              "status" : req.status
            }
          }
        )
      );
    } else if (req.status == 500) {
      this.dispatchEvent(
        new CustomEvent(
          "evtloginerror", {
            "detail": {
              "msg"    : 'Por favor, pruebe mas tarde...',
              "status" : req.status
            }
          }
        )
      );
    } else {
      console.log(req.status);
    }
    //this.dispatch('Error en el login');
  }

  clearInputFields() {
    this.email = undefined;
    this.password = undefined;
  }

}  //End de la clase


window.customElements.define('login-usuario', LoginUsuario);
