import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-input/paper-input';

import '@vaadin/vaadin-text-field/vaadin-text-field.js';
import '@vaadin/vaadin-text-field/vaadin-password-field.js';
import '@vaadin/vaadin-text-field/vaadin-email-field.js';

/**
 * @customElement
 * @polymer
 */
class AltaUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }
          @media (min-width: 1200px) {
            .container{
              max-width: 1200px;
            }
          }
    </style>
    <custom-style>
    <style is="custom-style">
      .custom-parent {
        font-size: 12px;
      }
  /*  paper-input.custom:hover {
        border: 1px solid #29B6F6;
      }*/
      paper-input.custom {
        margin-bottom: 14px;
        --primary-text-color: #01579B;
        --paper-input-container-color: black;
        --paper-input-container-focus-color: black;
        --paper-input-container-invalid-color: black;
        border: 1px solid #BDBDBD;
        border-radius: 5px;

        /* Reset some defaults */
        --paper-input-container: { padding: 0;};
        --paper-input-container-underline: { display: none; height: 0;};
        --paper-input-container-underline-focus: { display: none; };

        /* New custom styles */
        --paper-input-container-input: {
          box-sizing: border-box;
          font-size: inherit;
          padding: 4px;
        };
        /*--paper-input-container-input-focus: {
          background: rgba(0, 0, 0, 0.1);
        };
        --paper-input-container-input-invalid: {
          background: rgba(255, 0, 0, 0.3);
        };*/
        --paper-input-container-label: {
          top: -8px;
          left: 4px;
          background: white;
          padding: 2px;
          font-weight: bold;
        };
        --paper-input-container-label-floating: {
          width: auto;
        };
      }
    </style>
  </custom-style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <h2>Alta de un nuevo Usuario</h2>

<div class="container">
<div class="row">
<div class="col-md-12">
  <span hidden$="{{!existe}}">

  <div class="alert alert-danger alert-dismissible" role="alert-danger">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
   NO se ha podido dar de alta el correo indicado
   </div>
  </span>

  <span hidden$="{{!creado}}">
  <div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <a href="/login">
     <p class="mt-3 mb-3 text-muted">Usuario creado correctamente. Ya puede logarse con sus nuevas credenciales</p>
   </a>
   </div>
  </span>
<div class="row">
    <div class="custom-parent col-md-5 mb-3">
      <paper-input class="custom" id="first_name" label="Nombre" value="{{first_name::input}}"  always-float-label auto-validate required error-message="Es obligatorio introducir el nombre">
      </paper-input>
      </div>
        <div class="custom-parent col-md-5 mb-3">
      <paper-input class="custom" id="last_name" label="Apellidos" value="{{last_name::input}}" always-float-label required auto-validate error-message="Solo letras">
      </paper-input>
    </div>
</div>
<div class="row">
    <div class="custom-parent col-md-5 mb-3">
      <paper-input class="custom" type="email" id="email" label="Email:" value="{{email::input}}"
        value="{{email::input}}" invalid="{{invalidEmail}}" always-float-label auto-validate required error-message="El email es obligatorio">
      </paper-input>
      </div>
</div>
<div class="row">
    <div class="custom-parent col-md-5 mb-3">
      <paper-input class="custom" id="password" type="password" label="Password" required  value="{{password::input}}" invalid="{{invalidPassword}}" maxlength="10" always-float-label auto-validate  error-message="Password obligatorio">
      </paper-input>
      </div>
        <div class="custom-parent col-md-5 mb-3">
      <paper-input class="custom" id="r_password" type="password" label="Repetir Password" value="{{rpassword::input}}" invalid="{{invalidPassword}}" required maxlength="10" always-float-label auto-validate  error-message="Passowrd obligatorio">
      </paper-input>
    </div>
</div>
<div class="row">
    <div class="custom-parent col-md-3 mb-3">
      <paper-input class="custom" id="country" label="Pais" placeholder="country" value="{{country::input}}" required always-float-label auto-validate  error-message="Password obligatorio">
      </paper-input>
      </div>
        <div class="custom-parent col-md-3 mb-3">
      <paper-input class="custom" id="province" label="Provincia" placeholder="province" value="{{province::input}}" required always-float-label auto-validate  error-message="Passowrd obligatorio">
      </paper-input>
    </div>
    <div class="custom-parent col-md-4 mb-3">
  <paper-input class="custom" id="location" label="Localidad" placeholder="location" value="{{location::input}}" required always-float-label auto-validate  error-message="Passowrd obligatorio">
  </paper-input>
</div>
</div>

<div class="row">
    <div class="custom-parent col-md-8 mb-3">
      <paper-input class="custom" id="address" label="Dirección"  value="{{address::input}}" required always-float-label auto-validate  error-message="Password obligatorio">
      </paper-input>
      </div>
        <div class="custom-parent col-md-2 mb-3">
      <paper-input class="custom" id="postal_code" label="Codigo Postal" value="{{postal_code::input}}" required maxlength="5" always-float-label auto-validate  error-message="Passowrd obligatorio">
      </paper-input>
    </div>
</div>
<div class="row">
    <div class="custom-parent col-md-8 mb-3">
<button class="btn btn-lg btn-primary btn-block" on-click="registro">Registro de un usuario nuevo </button>
  </div>
</div>
</div>
</div>
</div>
  <!--  <form>
    <table>
    <p><label>Nombre: </label> <input type="first_name" placeholder="first_name" value="{{first_name::input}}" required></imput> </p>
    <tr><td><label>Apellidos: </label></td><td><input type="last_name" placeholder="last_name" value="{{last_name::input}}" required></imput> </p></td> </tr>

    <tr><td><p><label>Email: </label> </td> <td><input type="email" placeholder="email" required auto-validate error-message="Introduzca un email valido"
    value="{{email::input}}" invalid="{{invalidEmail}}"></input></td> </tr>
    <tr><td><label>Password: </label> </td> <td><input type="password" placeholder="password" required auto-validate error-message="Introduzca una password"
    value="{{password::input}}" invalid="{{invalidPassword}}" maxlength="10"></input> </p></td> </tr>

    <tr><td><label>Pais: </label> </td> <td><input type="country" placeholder="country"value="{{country::input}}" required></imput></p></td> </tr>
    <tr><td><label>Provincia: </label> </td> <td><input type="province" placeholder="province"value="{{province::input}}" required></imput></p></td> </tr>
    <tr><td><label>Localidad: </label> </td> <td><input type="location" placeholder="location"value="{{location::input}}" required></imput></p></td> </tr>
    <tr><td><label>Dirección: </label> </td> <td><input type="text" placeholder="address"value="{{address::input}}" required></imput></p></td> </tr>
    <tr><td><label>Codigo postal: </label> </td> <td><input type="postal_code" placeholder="postal_code"value="{{postal_code::input}}"required maxlength="5"></imput></p></td> </tr>
    </table>
    </form>-->



<!--     <button on-click="creacuenta">Crear cuenta nueva </button>  -->



   <iron-ajax
      id="doRegistro"
      url="http://localhost:3000/techu/v1/users"
      handle-as="json"
      method ="POST"
      content-type= "application/json"
      on-response="manageAJAXResponse"
      on-error="showError"
   >


    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String
      },last_name:{
        type:String
      },email: {
        type: String
      },password:{
        type:String
      },country:{
        type:String
      },province:{
        type:String
      },location:{
        type:String
      },address:{
        type:String
      },postal_code:{
        type:String
      },isLogged:{
        type:Boolean,
        value:false
      },existe:{
        type:Boolean,
        value:false
      },creado:{
        type:Boolean,
        value:false
      }
    };
  } // Fin de prpperties



  _validateForm() {
    this.shadowRoot.querySelector('#first_name').validate();
           this.shadowRoot.querySelector('#last_name').validate();
           this.shadowRoot.querySelector('#email').validate();
           this.shadowRoot.querySelector('#password').validate();
           this.shadowRoot.querySelector('#r_password').validate();
           this.shadowRoot.querySelector('#country').validate() ;
           this.shadowRoot.querySelector('#province').validate();
           this.shadowRoot.querySelector('#location').validate();
           this.shadowRoot.querySelector('#address').validate();
           this.shadowRoot.querySelector('#postal_code').validate();

    return this.shadowRoot.querySelector('#first_name').validate() &&
           this.shadowRoot.querySelector('#last_name').validate() &&
           this.shadowRoot.querySelector('#email').validate() &&
           this.shadowRoot.querySelector('#password').validate() &&
           this.shadowRoot.querySelector('#r_password').validate() &&
           this.shadowRoot.querySelector('#country').validate() &&
           this.shadowRoot.querySelector('#province').validate() &&
           this.shadowRoot.querySelector('#location').validate() &&
           this.shadowRoot.querySelector('#address').validate() &&
           this.shadowRoot.querySelector('#postal_code').validate()
           ;
  }


 registro(){
  console.log("El Usuario ha pulsado el boton");
  console.log("voy a enviar la peticion");
  if (this._validateForm()) {
    var datoshome={
      "country": this.country,
      "province": this.province,
      "location": this.location,
      "address": this.address,
      "postal_code": this.postal_code,
      "geolocation": {
          "longitude": 40.5139126,
         	"latitude": -3.6742344
      }
    }
    var loginData ={
      "first_name": this.first_name,
      "last_name": this.last_name,
      "email" : this.email,
      "password" : this.password,
      "type": "CUSTOMER",
      "home" : datoshome
    }

    console.log(loginData);

    this.$.doRegistro.body = JSON.stringify(loginData);  //lo trasnformo en json los datos
    this.$.doRegistro.generateRequest();
    console.log ("Peticion de registro de nuevo usuario enviada");
  }
 }

  showError(error){
    console.log("Hubo un error en el resgistro del usuario");
    console.log(error);
    const req = error.detail.request;

    if (req.status == 409){
      //this.existe=true;
      
      this.dispatchEvent(
        new CustomEvent(
          "evtaltaerror", {
            "detail": {
              "msg"    : 'No se ha podido dar de alta el usuario indicado',
              "status" : req.status
            }
          }
        )
      );
    }
  }

  manageAJAXResponse(data){
     console.log("manageAJAXResponse");
     console.log(data.detail.response);

     if (data.detail.response.idUsuario==0){
       //this.existe=true;
       console.log(this.existe);
       this.dispatchEvent(
          new CustomEvent(
            "evtaltaerror", {
              "detail": {
                "msg"    : 'No se ha podido dar de alta el usuario indicado',
                "status" : 409
              }
            }
          )
        );
     }
     else{
       //this.creado =true;
       this.dispatchEvent(
         new CustomEvent(
           "evtaltasusceed",
           {
             "detail": {
               "msg": 'Usuario creado correctamente. Ya puede logarse con sus nuevas credenciales',
               "user": {
                 "userId"    : data.detail.response.id,
                 "userType"  : "CUSTOMER",
                 "email"     : this.email
               }
             }
           }
         )
       );
     }
  }
}  //End de la clase


window.customElements.define('alta-usuario', AltaUsuario);
