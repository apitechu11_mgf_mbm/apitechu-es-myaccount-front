import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {} from '@polymer/polymer/lib/elements/dom-if.js';

import '@polymer/iron-ajax/iron-ajax';
import '@polymer/iron-icons/iron-icons';
import '@polymer/iron-pages/iron-pages';
import '@polymer/app-route/app-route';
import '@polymer/app-route/app-location';
import '@polymer/paper-icon-button/paper-icon-button';
import '@polymer/paper-input/paper-input';
import '@polymer/paper-toast/paper-toast';
import '@polymer/app-layout/app-header-layout/app-header-layout';
import '@polymer/app-layout/app-header/app-header';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout';
import '@polymer/app-layout/app-drawer/app-drawer';
import '@polymer/app-layout/app-toolbar/app-toolbar';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects';

import '../login-usuario/login-usuario.js';
import '../alta-usuario/alta-usuario.js';
import '../forget-passwd/forget-passwd.js'

import '../logout-usuario/logout-usuario.js';
import '../visor-usuario/visor-usuario.js';


import '../transfe-cuentas/transfe-cuentas.js'
import '../ingreso-cuenta/ingreso-cuenta.js'
import '../reintegro-cuenta/reintegro-cuenta.js'

/**
 * @customElement
 * @polymer
 */
class AppMain extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

        app-header {
          color: #fff;
          background-color: #003DB3;

          --app-header-background-front-layer: {
            background-image: url('/resources/img/login-background.jpg');
            background-position: 50% 10%;
          }
        }
        .user {
          font-size: 17px;
        }
        paper-icon-button {
          --paper-icon-button-ink-color: white;
        }
        .menu {
          padding-left: 16px;
        }
        ul {
          list-style: none;
        }

        .outer {
          width: 100%;
          height: 100%;
          @apply(--layout-vertical);
          @apply(--layout-flex);
        }

        .inner {
          @apply(--layout-flex);
          @apply(--layout-vertical);
        }

        .content {
          @apply(--layout-flex);
        }

        #toastError {
          --paper-toast-background-color: #CC0000;
          --paper-toast-color: white;
          --paper-font-common-base: {
            font-size: 20px;
          }
        }

        #toastInfo {
          --paper-toast-background-color: #007E33;
          --paper-toast-color: white;
          --paper-font-common-base: {
            font-size: 20px;
          }
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <app-location route="{{route}}"></app-location>

      <app-route
        route="{{route}}"
        pattern ="/:resource"
        data="{{routeData}}"
      ></app-route>

      <app-route
        route="{{route}}"
        pattern ="/:resource/:id"
        data="{{routeData}}"
      ></app-route>

      <app-drawer-layout force-narrow>

        <app-drawer opened="{{drawerOpenedAppMain}}" slot="drawer">
          <div class="menu">
            <h3>MyAccounts</h3>
            <span hidden$="{{isLogged}}">
              <p><a class="list-group-item" href="/login" on-click="doViewLogin">Inicio sesión</a></p>
            </span>
            <span hidden$="{{!isLogged}}">
              <p><a class="list-group-item" href="/session/[[user.userId]]" on-click="doViewAccounts">Posición Global</a></p>
              <p><a class="list-group-item" href="/session/[[user.userId]]" on-click="doNewAccount">Crear nueva cuenta</a></p>
              <p><a class="list-group-item" href="/logout/[[user.userId]]" on-click="doViewLogout">Salir</a></p>
              <!-- p><a class="list-group-item" href="/session/[[user.userId]]" on-click="doViewMovements">Mis Movimientos</a></p -->
              <!-- ul><li <a class="list-group-item" href="/transfe/[[user.userId]]" on-click="doTransfer">Hacer una transferencia</a></li></ul -->
              <!-- ul><li <a class="list-group-item" href="/income/[[user.userId]]" on-click="doIncome">Hacer un Ingreso</a></li></ul -->
              <!-- ul><li <a class="list-group-item" href="/refund/[[user.userId]]" on-click="doRefund">Hacer un Reintegro</a></li></ul -->
            </span>
          </div>
        </app-drawer>

        <app-header-layout>
          <app-header condenses fixed effects="blend-background parallax-background resize-title" slot="header">
            <app-toolbar>
              <!-- span hidden$="{{!isLogged}}">
                <paper-icon-button icon="icons:arrow-back" title="Atrás" on-click="onBack"></paper-icon-button>
              </span -->

              <paper-icon-button icon="icons:menu" title="Menú" on-click="onMenu"></paper-icon-button>

              <!-- span hidden$="{{!isLogged}}">
                <paper-icon-button icon="icons:account-balance" title="Ver cuentas" on-click="doViewAccounts"></paper-icon-button>
              </span -->

              <div condensed-title>[[title]] BBVA</div>
              <label class="user">[[user.first_name]] [[user.last_name]]<br>[[user.email]]<br>[[user.userType]]</label>
              <!-- span hidden$="{{isLogged}}">
                <paper-icon-button icon="icons:settings-power" title="Inicio Sesión" on-click="doViewLogin"></paper-icon-button>
              </span -->

              <!-- span hidden$="{{!isLogged}}">
                <paper-icon-button icon="icons:power-settings-new" title="Salir de MyAccounts" on-click="doViewLogout"></paper-icon-button>
              </span -->
            </app-toolbar>

            <app-toolbar></app-toolbar>

            <app-toolbar>
              <div main-title spacer>[[title]]</div>
            </app-toolbar>
          </app-header>

          <iron-pages selected="[[routeData.resource]]" attr-for-selected="component-name">
            <div component-name="login">
              <login-usuario id="login" 
                on-evtloginsusceed="processLogin" 
                on-evtloginerror="processLoginError"
              ></login-usuario>
            </div>

            <div component-name="newuser">
              <alta-usuario id="newuser" userid="[[routeData.id]]"
                on-evtaltasusceed="processAlta"
                on-evtaltaerror="processAltaError"
              ></alta-usuario>
            </div>

            <div component-name="forgetpassword">
              <forget-passwd id="forgetpassword" userid="[[routeData.id]]" 
                on-evtresetsusceed="processReset" 
                on-evtreseterror="processResetError"
              ></forget-passwd>
            </div>

            <div component-name="session">
              <visor-usuario id="visor" component="[[pageSession]]" userid="[[routeData.id]]" 
                on-evtdetailusersusceed="processDetailUser"
                on-evtnewaccountsusceed="processNewAccount"
                on-evtnewaccounterror="processNewAccountError"
                on-evtingresosusceed="processIngresoOK"
                on-evtreintegrosusceed="processReintegroOK"
                on-evttransfersusceed="processTransferOK"
              ></visor-usuario>
            </div>

            <div component-name="logout">
              <logout-usuario id="logout" userid="[[routeData.id]]" 
                on-evtlogoutsusceed="processLogout"
              ></logout-usuario>
            </div>
          </iron-pages>
        </app-header-layout>
      </app-drawer-layout>

      <paper-toast id="toastInfo" class="fit-bottom" duration=5000></paper-toast>
      <paper-toast id="toastError" class="fit-bottom" duration=5000></paper-toast>
    `
  } // Fin de template

  static get properties() {
    return {
      route:{
        type : Object
      },
      routeData :{
        type: Object
      },
      isLogged: {
        type: Boolean,
        value: false,
        statePath: 'isLogged'
      },
      userToken: {
        type: String
      },
      user: {
        type: Object
      },
      pageSession:{
        type : String
      },
      initialized:{
        type: Boolean
      },
      title: {
        type: String,
        computed: 'computeTitle(routeData)'
      },
      visible: {
        type: Boolean,
        value: false,
        observer: '_visibleChanged'
      },
      drawerOpenedAppMain: {
        type: Boolean,
        value: false
      }
    };
  } // Fin de properties

  constructor() {
    super();
    this.initialized = false;
    console.log("AppMain.constructor");
  }

  static get observers() {
    return [
      '_loginChanged(isLogged)',
      '_pathChanged(route.path)',
    ]
  }

  connectedCallback() {
    if (!this.initialized) {
      this.user = JSON.parse(window.sessionStorage.getItem('user'));
      this.userToken = window.sessionStorage.getItem('userToken');
      this.isLogged = window.sessionStorage.getItem('isLogged');

      document.addEventListener('toastError', (e) => {
        this.$.toastError.text = e.detail;
        this.$.toastError.open();
      });

      document.addEventListener('toastInfo', (e) => {
        this.$.toastInfo.text = e.detail;
        this.$.toastInfo.open();
      });

      this.initialized = true;
      console.log("AppMain.connectedCallback initialized");
    }
    super.connectedCallback();
    console.log("AppMain.connectedCallback");
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    if (this.initialized) {
      document.removeEventListener('toastError');
      document.removeEventListener('toastInfo');
      this.initialized = false;
      console.log("AppMain.disconnectedCallback not initialized");
    }
    console.log("AppMain.disconnectedCallback");
  }

  _loginChanged(isLogged) {
    if (!isLogged && this._isSessionPath()) {
      this.set('route.path', '/login/');
    } else if (isLogged && !this._isSessionPath()) {
      this.set('route.path', '/session/');
    }
  }

  _pathChanged() {
    console.log('AppMain._pathChanged');
    console.log(this.route);
    console.log(this.routeData);
    //this.pageSession = this.routeData.resource;

    if (this.isLogged && !this._isSessionPath()) {
      this.set('route.path', '/session/');
    }
  }

  _isSessionPath() {
    /*return this.route.path.includes('session') || 
           this.route.path.includes('transfe') ||
           this.route.path.includes('refund') ||
           this.route.path.includes('income'); */
    return this.route.path.includes('session');
  }

  attributeChangedCallback(name, oldVal, newVal) {
    console.log("AppMain.attributeChangedCallback");
    //this[name] = newVal;
  }

  computeTitle(routeData) {
    console.log("computeTitle: " + routeData.resource);

    if (!this.isLogged) {
      return 'MyAccounts';
    } else {
      switch (routeData.resource) {
        case 'session':
        case 'transfe':
        case 'refund':
        case 'income':
          return 'MyAccounts';
        case 'login':
          return 'MyAccounts';
        case 'logout':
          return 'MyAccounts';
        default:
          return 'MyAccounts';
      }
    }
  }
  
  onMenu() {
    console.log('Menu');
    this.drawerOpenedAppMain = !this.drawerOpenedAppMain;
  }

  doViewLogin () {
    console.log('doViewLogin');
    this.drawerOpenedAppMain = false;
    this.set('route.path', '/login');
  }

  doViewAccounts() {
    console.log('doViewAccounts');
    this.drawerOpenedAppMain = false;
    this.pageSession = "miscuentas";
    this.set('route.path', '/session');
  }

  doNewAccount() {
    console.log('doNewAccount');
    this.drawerOpenedAppMain = false;
    this.pageSession = "crearcuenta";
    this.set('route.path', '/session');
  }

  doViewMovements() {
    console.log('doViewMovements');
    this.drawerOpenedAppMain = false;
    this.pageSession = "mismovimientos";
    this.set('route.path', '/session');
  }

  doViewLogout () {
    console.log('doViewLogout');
    this.drawerOpenedAppMain = false;
    this.set('route.path', '/logout');
  }

  doTransfer(){
    console.log('doTransfer');
    this.drawerOpenedAppMain = false;
    this.set('route.path', '/transfe');
  }

  doRefund(){
    console.log('doRefund');
    this.drawerOpenedAppMain = false;
    this.set('route.path', '/refund');
  }

  doIncome(){
    console.log('doIncome');
    this.drawerOpenedAppMain = false;
    this.set('route.path', '/income');
  }

  processLogin (event) {
    console.log("AppMain.processLogin");
    console.log(event.detail);

    this.user = event.detail.user;
    this.userToken = event.detail.userToken;
    this.isLogged = event.detail.isLogged;

    window.sessionStorage.setItem('user', JSON.stringify(this.user));
    window.sessionStorage.setItem('userToken', this.userToken);
    window.sessionStorage.setItem('isLogged', this.isLogged);

    window.location = "/session/"+this.user.userId;
  } // End processEvent

  processLoginError (event) {
    console.log("AppMain.processLoginError");
    console.log(event.detail);

    this.user = null;
    this.isLogged = false;

    window.sessionStorage.clear();
    this.$.toastError.text = event.detail.msg;
    this.$.toastError.open();
  } // End processLoginError

  processAlta (event) {
    console.log("AppMain.processAlta");
    console.log(event.detail);

    this.user = null;
    this.isLogged = false;

    window.sessionStorage.clear();
    this.$.toastInfo.text = event.detail.user.email + ' ' + event.detail.msg;
    this.$.toastInfo.open();

  } // End processAlta

  processAltaError (event) {
    console.log("AppMain.processAltaError");
    console.log(event.detail);

    this.user = null;
    this.isLogged = false;

    window.sessionStorage.clear();
    this.$.toastError.text = event.detail.msg;
    this.$.toastError.open();
  } // End processAltaError

  processDetailUser(event) {
    console.log("AppMain.processDetailUser");
    console.log(event.detail);

    this.user = event.detail.user;
    this.isLogged = event.detail.isLogged;
  } // End processDetailUser

  processNewAccount (event) {
    console.log("AppMain.processNewAccount");
    console.log(event.detail);

    this.$.toastInfo.text = event.detail.msg;
    this.$.toastInfo.open();
  } // End processAltaError

  processNewAccountError (event) {
    console.log("AppMain.processNewAccountError");
    console.log(event.detail);

    this.$.toastError.text = event.detail.msg;
    this.$.toastError.open();
  } // End processAltaError

  processLogout (event) {
    console.log("AppMain.processLogout");
    console.log(event.detail);

    this.user = null;
    this.isLogged = false;

    window.sessionStorage.clear();
  } // End processLogout

  processReset(event) {
    console.log("AppMain.processReset");
    console.log(event.detail);

    this.user = null;
    this.isLogged = false;

    window.sessionStorage.clear();
    this.$.toastInfo.text = event.detail.user + ' ' + event.detail.msg;
    this.$.toastInfo.open();
  } // End processReset

  processResetError (event) {
    console.log("AppMain.processResetError");
    console.log(event.detail);

    this.user = null;
    this.isLogged = false;

    window.sessionStorage.clear();
    this.$.toastError.text = event.detail.msg;
    this.$.toastError.open();
  } // End processResetError
  
  processIngresoOK(event) {
    console.log("AppMain.processIngresoOK");
    console.log(event.detail);

    this.$.toastInfo.text = event.detail.msg;
    this.$.toastInfo.open();
  } // End processIngresoOK

  processReintegroOK(event) {
    console.log("AppMain.processReintegroOK");
    console.log(event.detail);

    this.$.toastInfo.text = event.detail.msg;
    this.$.toastInfo.open();
  } // End processReintegroOK

  processTransferOK(event) {
    console.log("AppMain.processTransferOK");
    console.log(event.detail);

    this.$.toastInfo.text = event.detail.msg;
    this.$.toastInfo.open();
  } // End processTransferOK

}

window.customElements.define('app-main', AppMain);
