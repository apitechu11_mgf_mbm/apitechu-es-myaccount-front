import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-input/paper-input';

import '../login-usuario/login-usuario.js';
import '../visor-usuario/visor-usuario.js';
import '../logout-usuario/logout-usuario.js';

/**
 * @customElement
 * @polymer
 */
class PanelUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <span hidden$="{{isLogged}}"><login-usuario id="login" on-evtloginsusceed="processLogin" on-evtloginerror="processLoginError"></login-usuario></span>
      <span hidden$="{{!isLogged}}">
        <visor-usuario id="visor"></visor-usuario>
        <logout-usuario id="logout" on-evtlogoutsusceed="processLogout"></logout-usuario>
      </span>
    `;
  } // End template

  static get properties() {
    return {
      isLogged: {
        type: Boolean,
        value: false
      },
      userType: {
        type: String,
        value: ""
      }
    };
  } // End properties

  processLogin (event) {
    console.log("Capturado evento processLogin del emisor");
    console.log(event.detail);

    this.$.visor.id = event.detail.userId;
    this.$.visor.token = event.detail.userToken;
    this.isLogged = event.detail.isLogged;
    this.userType = event.detail.userType;
    this.$.logout.id = event.detail.userId;
    this.$.logout.token = event.detail.userToken;
  } // End processEvent

  processLoginError (event) {
    console.log("Capturado evento processLoginError del emisor");
    console.log(event.detail);

    this.isLogged = false;
    this.userType = "";
  } // End processLoginError

  processLogout (event) {
    console.log("Capturado evento processLogout del emisor");
    console.log(event.detail);

    this.isLogged = event.detail.isLogged;
    this.userType = event.detail.userType;
  } // End processLogout

} // End class

window.customElements.define('panel-usuario', PanelUsuario);
