import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
/**
 * @customElement
 * @polymer
 */
class VisorMov extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <div class="container">
      <div class="row">
      <div class="col-md-12">
  
      <h2>Movimientos de la cuenta <strong>{{iban}}</strong></h2>
  
      <span hidden$="{{!sierrmvtos}}">
        <h3>No existen movimientos en la cuenta</h3>
      </span>

      <vaadin-grid id="movimientos" height-by-rows theme="row-stripes">
        <vaadin-grid-column path="Fecha" width="15%" text-align="center" header="Fecha"></vaadin-grid-column>
        <vaadin-grid-column path="Concepto" width="30%" flex-grow="0"  header="Concepto del movimiento"></vaadin-grid-column>
        <vaadin-grid-column path="Importe" width="15%" text-align="end" header="Importe"></vaadin-grid-column>
        <vaadin-grid-column path="Saldo" width="15%" text-align="end" header="Saldo"></vaadin-grid-column>
      </vaadin-grid>

      <!-- dom-repeat items="{{movimientos}}">
        <template>
          <h3>Fecha - {{item.Fecha}} - Concepto - {{item.Concepto}} -Importe - {{item.Importe}} - Saldo - {{item.Saldo}}</h3>
        </template>
      </dom-repeat -->

      </div>
      </div>
      </div>

      <iron-ajax
        id="getMovements"
        headers="[[headers]]"
        url="http://localhost:3000/techu/v1/movement/{{userid}}/{{iban}}"
        handle-as="json"
        method="GET"
        content-type="application/json"
        on-response="tratarRespuestaMovimientos"
        on-error="tratarErrorMovimientos"
      ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      userid: {
        type: String,
        observer: '_useridChanged'
      },
      token: {
        type: String
      },
      headers: {
        computed: '_computeHeader(token)',
        observer: "_headersChanged"
      },
      iban:{
        type:String,
        observer: '_ibanChanged'
      },
      movimientos: {
        type: Array
      },
      sierrmvtos:{
        type:Boolean,
        value:true
      }
    };
  }

  constructor() {
    console.log("VisorMov.constructor");
    super();
  }

  connectedCallback() {
    console.log("VisorMov.connectedCallback");
    super.connectedCallback();
    const user = JSON.parse(window.sessionStorage.getItem('user'));
    console.log(user);
    if (user != null) {
      this.userid = user.userId;
    }
    this.token = window.sessionStorage.getItem('userToken');
  }

  disconnectedCallback() {
    console.log("VisorMov.disconnectedCallback");
    super.disconnectedCallback();
  }

  _computeHeader(token) {
    console.log("VisorMov._computeHeader");
    console.log(token);
    if (token == undefined) {
      return null;
    } else {
      var header = {
        jwt: token
      }
      console.log(header);
      return header;
    }
  }

  _headersChanged(newValue, oldValue) {
    console.log("VisorMov._headersChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);
    if (newValue != undefined) {
      this.sincronizaPeticion(newValue, this.userid, this.iban);
    }
  } // End _headersChanged (observer)

  _useridChanged(newValue, oldValue) {
    console.log("VisorMov._useridChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);
    if (newValue != undefined) {
      this.sincronizaPeticion(this.headers, newValue, this.iban);
    }
  } // End _useridChanged (observer)

  _ibanChanged(newValue, oldValue) {
    console.log("VisorMov._ibanChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);
    if (newValue != undefined) {
      this.sincronizaPeticion(this.headers, this.userid, newValue);
    }
  } // End _ibanChanged (observer)

  sincronizaPeticion(headers, userid, iban) {
    console.log("VisorMov.sincronizaPeticion");
    console.log(headers);
    console.log(userid);
    console.log(iban);

    if (headers == undefined) {
      this.token = window.sessionStorage.getItem('userToken');
    } else {
      if (headers != undefined && this.headers == headers 
        && userid != undefined && this.userid == userid
        && iban != undefined && this.iban == iban) {
        this.$.getMovements.generateRequest();
        console.log("Petición enviada");
      }
    }
  }

  tratarRespuestaMovimientos(data) {
    console.log("VisorMov.tratarRespuestaMovimientos");
    console.log(data.detail.response);

    this.movimientos = data.detail.response;
    this.sierrmvtos = false;

    this.$.movimientos.items = this.movimientos;
  } // End tratarRespuestaMovimientos

  tratarErrorMovimientos(error) {
    console.log("VisorMov.tratarErrorMovimientos");
    console.log(error);

    this.movimientos = "";
    this.sierrmvtos = true;
  } // End tratarErrorMovimientos

}

window.customElements.define('visor-mov', VisorMov);
