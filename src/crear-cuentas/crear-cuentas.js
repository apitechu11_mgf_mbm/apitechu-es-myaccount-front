import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
/**
 * @customElement
 * @polymer
 */
class CrearCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }

   }

      </style>
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
       <div class="container">
       <div class="row">
       <div class="col-md-12">
   
       <h2>Crear una nueva cuenta</h2>
 
       <form id="form1" class="needs-validation" novalidate>
        <p>
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="alias">Alias de la cuenta</label>
            <input type="text" class="form-control" id="alias" placeholder="Alias de la cuenta" value="{{alias::input}}" required>
            <div class="invalid-feedback">Un alias valido es requerido.</div>
          </div>
        </p>

        <div class="col-md-5 mb-3">
          <label for="divisa">Seleccione la divisa</label>
          <select class="custom-select d-block w-100" id="seldivisa" value="{{divisa::change}}" required>
            <option value="EUR"> Euros</option>
            <option value="USD"> Dólares</option>
            <option value="YEN"> Yenes</option>
          </select>
        </div>
        <button class="btn btn-primary btn-lg btn-block" on-click="crearAccount">Crear cuenta</button>
      </form>

      </div>
      </div>
      </div>

      <iron-ajax
        id="createAccount"
        headers="[[headers]]"
        url="http://localhost:3000/techu/v1/accounts/{{userid}}"
        handle-as="json"
        method="POST"
        content-type="application/json"
        on-response="tratarRespuestaAltaCuenta"
        on-error="tratarErrorAltaCuenta"
      ></iron-ajax>
    `;
  }

  static get properties() {
    return {
      userid: {
        type: String,
        observer: '_useridChanged'
      },
      token: {
        type: String
      },
      headers: {
        computed: '_computeHeader(token)',
        observer: "_headersChanged"
      },
      id : {
        type: String
      },
      alias : {
        type: String
      },
      divisa : {
        type: String
      }
    };
  } // Fin de prpperties

  constructor() {
    console.log("CrearCuentas.constructor");
    super();
  }

  connectedCallback() {
    console.log("CrearCuentas.connectedCallback");
    super.connectedCallback();
    const user = JSON.parse(window.sessionStorage.getItem('user'));
    console.log(user);
    if (user != null) {
      this.userid = user.userId;
    }
    this.token = window.sessionStorage.getItem('userToken');
  }

  disconnectedCallback() {
    console.log("CrearCuentas.disconnectedCallback");
    super.disconnectedCallback();
  }

  _computeHeader(token) {
    console.log("CrearCuentas._computeHeader");
    console.log(token);
    if (token == undefined) {
      return null;
    } else {
      var header = {
        jwt: token
      }
      console.log(header);
      return header;
    }
  }

  _headersChanged(newValue, oldValue) {
    console.log("CrearCuentas._headersChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);
    /*if (newValue != undefined) {
      this.sincronizaPeticion(newValue, this.userid);
    }*/
  } // End _headersChanged (observer)

  _useridChanged(newValue, oldValue) {
    console.log("CrearCuentas._useridChanged");
    console.log("Old value: " + oldValue);
    console.log("New value: " + newValue);
    /*if (newValue != undefined) {
      this.sincronizaPeticion(this.headers, newValue);
    }*/
  } // End _useridChanged (observer)

  /*sincronizaPeticion(headers, userid) {
    console.log("CrearCuentas.sincronizaPeticion");
    console.log(headers);
    console.log(userid);

    if (headers == undefined) {
      this.token = window.sessionStorage.getItem('userToken');
    } else {
      if (headers != undefined && this.headers == headers 
        && userid != undefined && this.userid == userid) {
        this.$.getMovements.generateRequest();
        console.log("Petición enviada");
      }
    }
  }*/

  crearAccount(){
    console.log("CrearCuentas.crearAccount");

    if (this.divisa == undefined){
      this.divisa = "EUR";
    }

    var accountData ={
      "alias" : this.alias,
      "divisa" : this.divisa,
      "entidad": "0182",
      "oficina": "3000"
    }
    console.log(accountData);

    this.$.createAccount.body = JSON.stringify(accountData);  //lo trasnformo en json los datos
    console.log("datos de la cuenta a crear:");
    console.log(this.$.createAccount.body);
    this.$.createAccount.generateRequest();
    console.log("Petición enviada");
  }

  tratarRespuestaAltaCuenta(data) {
    console.log("CrearCuentas.tratarRespuestaAltaCuenta");
    console.log(data.detail.response);
    //this.existe = true;
    this.dispatchEvent(
      new CustomEvent(
        "evtnewaccountsusceed",
        { 
          "detail": {
            "msg": data.detail.response.msg,
            "iban": data.detail.response.iban
          }
        }
      )
    );
    //this.$.form1.reset();
  }

  tratarErrorAltaCuenta(error) {
    console.log("CrearCuentas.tratarErrorMail");
    console.log(error);

    const req = error.detail.request;
    console.log("veo " + req.status);

    if (req.status == 400 || req.status == 401 || req.status == 403) {
      this.dispatchEvent(
        new CustomEvent(
          "evtnewaccounterror", {
            "detail": {
              "msg"    : 'Error al dar de alta la cuenta ...',
              "status" : req.status
            }
          }
        )
      );
    } else if (req.status == 500) {
      this.dispatchEvent(
        new CustomEvent(
          "evtnewaccounterror", {
            "detail": {
              "msg"    : 'Por favor, pruebe mas tarde...',
              "status" : req.status
            }
          }
        )
      );
    } else {
      console.log(req.status);
    }
    //this.$.form1.reset();
  }

  manageAJAXResponse(data){
    console.log("manageAJAXResponse");
    console.log(data.detail.response);
    this.transferido = true;
  }

}

window.customElements.define('crear-cuentas', CrearCuentas);
